# Simple Fintech (e-Wallet) Application #

MVP (Minimum Viable Product) E-Wallet Application is simple backend used for cashless payment app based.

# Backend Tech
    - Java
    - PostgreSQL
    - Docker
    - Spring Boot Framework

# Output
    - REST API for Frontend Developer

# Application Features
    1. Customers can register
    2. As an owner you can register a new merchant who can make transactions
    3. Customers can fill in the balance (Topup)
    4. Customers can make payments to registered merchants 

# Technical Requirements 
    Entity
    1. (entity) user_accounts
        1.a. user_id
        1.b. name
        1.c. email
        1.d. phonenumber
        1.e. user_type (merchant | customer)
    2. (entity) wallet
        2.a. wallet_id
        2.b. user_id
        2.c. Balance
    3. (entity) transaksi
        3.a. id
        3.b. reference
        3.c. date
        3.d. description
        3.e. type (topup | payment)
        4.f. status
    4. Topup:
        4.a. credit wallet
    5. Payment:
        5.a. credited wallet (merchant walletid)
        5.b. debited wallet (customer walletid)

    Services
    1. register_customer | register_merchant =
        1.a. add new user to user_accounts
        1.b. add new wallet to wallet
    2. topup:
        2.a. add new transaksi with type topup
        2.b. increase balance from wallet
    3. payment:
        3.a. add new transaksi with type payment
        3.b. decrease balance from debited wallet
        3.c. increase balance from credited wallet

# How To Start Database #
    STEP 1
     1. Running PostgreSQL in Docker
        For UNIX RUN via TERMINAL :
        $ docker run --rm \
        --name ewallet-db \
        -e POSTGRES_DB=ewalletdb \
        -e POSTGRES_USER=ewallet \
        -e POSTGRES_PASSWORD=aT8XQjId \
        -e PGDATA=/var/lib/postgresql/data/pgdata \
        -v "$PWD/ewalletdb-data:/var/lib/postgresql/data" \
        -p 5432:5432 \
        postgres:14
        
        For WINDOWS RUN via COMMAND PROMPT :
        docker run --rm ^
        --name ewallet-db ^
        -e POSTGRES_DB=ewalletdb ^
        -e POSTGRES_USER=ewallet^
        -e POSTGRES_PASSWORD=aT8XQjId ^
        -e PGDATA=/var/lib/postgresql/data/pgdata ^
        -v “%cd%\ewalletdb-data:/var/lib/postgresql/data” ^
        -p 5432:5432 ^
        postgres:14

