create table user_accounts (
  id varchar(36),
  primary key (id)
);

create table wallet (
  id varchar(36),
  primary key (id)
);

create table topup (
  id varchar(36),
  primary key (id)
);

create table payment (
  id varchar(36),
  primary key (id)
);

create table transaction (
  id varchar(36),
  primary key (id)
);

